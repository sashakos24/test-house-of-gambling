import React from 'react';
import styles from './Home.module.scss';
import Header from "../../components/header/Header";
import Banner from "../../components/banner/Banner";
import Footer from "../../components/footer/Footer";
import MediaQuery from "react-responsive";
import HeaderMobile from "../../components/header/HeaderMobile/HeaderMobile";

const Home = () => {


    return (
        <div className={styles.home}>
            <div className={styles.ellipse} />
            <div className={styles.content}>
                <MediaQuery query="(min-width: 760px)">
                    <Header/>
                </MediaQuery>
                <MediaQuery query="(max-width: 760px)">
                    <HeaderMobile/>
                </MediaQuery>
                <Banner/>
                <Footer/>
            </div>
            <div className={styles.circle} />
        </div>
    );
};

export default Home;
