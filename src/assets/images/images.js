import lang_ic from './icons/language.png';
import social_circle from './icons/social_network.png';
import social_active from './icons/social_network_active.png';
import twitter_ic from './icons/twitter.png';
import facebook_ic from './icons/facebook.png';
import linked_ic from './icons/linkedIn.png';
import instagram_ic from './icons/instagram.png';
import tiktok_ic from './icons/tiktok.png';
import vk_ic from './icons/vk.png';
import youtube_ic from './icons/youtube.png';
import telegram_ic from './icons/telegram.png';

export {
    lang_ic,
    social_circle,
    social_active,
    twitter_ic,
    youtube_ic,
    telegram_ic,
    vk_ic,
    facebook_ic,
    instagram_ic,
    linked_ic,
    tiktok_ic
}
