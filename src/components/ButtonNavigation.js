import React from 'react';
import styles from './Components.module.scss';

const ButtonNavigation = ({children, fill, style, onClick, href, className}) => {
    return (
        <a href={href || ''} onClick={onClick} style={style} className={(fill ? styles.button_fill : '') + ' '  + styles.button + ' ' + className}>
            {children}
        </a>
    );
};

export default ButtonNavigation;
