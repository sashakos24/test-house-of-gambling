import React from 'react';
import styles from './Banner.module.scss';
import ButtonNavigation from "../ButtonNavigation";
import NewsItem from "./NewsItem";

const Banner = () => {
    const news = [
        {
            title: 'Certain categories of heirs under the law',
            date: new Date(2023, 8, 21),
            href: 'https://sibac.info/conf/jurisprudence/lxxiii/300040'
        },
        {
            title: 'Mental arithmetic in computer science lessons at school',
            date: new Date(2023, 8, 23),
        },
        {
            title: 'Ecological advancements in brazil during the 2010s',
            date: new Date(2023, 8, 24),
        },
        {
            title: 'Social networks showed a recipe for a fat-burning drink. Let\'s analyze it together with a nutritionist',
            date: new Date(2023, 4, 13),
        },
        {
            title: 'Microsoft has filed a patent application for a smart backpack with artificial intelligence',
            date: new Date(2023, 9, 1),
        },
    ]

    return (
        <div className={styles.banner}>
            <div className={styles.content}>
                <div className={styles.title_block}>
                    <h1 className={styles.title}>
                        HOUSE OF <span className={styles.blue}>GAMBLING</span>
                    </h1>
                    <h3 className={styles.subtitle}>
                        Raise your ROI with direct advertiser
                    </h3>
                    <ButtonNavigation className={styles.btn_partner} fill={true} href={'#'}>
                        Become a Partner
                    </ButtonNavigation>
                </div>
                <div className={styles.banner_image}>
                    <div className={styles.image_block}>
                        <div className={styles.gradient}/>
                    </div>
                </div>
                <div className={styles.blog}>
                    <h2 className={styles.blog_title}>
                        BLOG
                    </h2>
                    <div className={styles.blog_content}>
                        <div className={styles.blog_items}>
                            {news.map((article, key) =>
                                <NewsItem key={key} date={article.date} text={article.title} href={article.href}/>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Banner;
