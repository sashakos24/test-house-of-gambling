import React from 'react';
import styles from './Banner.module.scss';

const NewsItem = ({text, date, href, key}) => {
    return (
        <a key={key} className={styles.news_item} href={href || '#'} target={href ? '_blank' : ''}>
            <h4 className={styles.news_text}>
                {text}
            </h4>
            <div className={styles.news_date}>
                {date.toLocaleDateString().replaceAll('/', '.')}
            </div>
        </a>
    );
};

export default NewsItem;
