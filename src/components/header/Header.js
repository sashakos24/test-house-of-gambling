import React, {useEffect, useState} from 'react';
import styles from './Header.module.scss';
import ButtonNavigation from "../ButtonNavigation";
import {lang_ic} from '../../assets/images/images';
import {links} from "../../assets/utils/constants";

const Header = () => {

    const languages = {
        en: 'EN',
        ru: 'RU'
    }
    const [lang, setLang] = useState(languages.en);
    useEffect(() => {
        if (localStorage.hasOwnProperty('lang')) {
            setLang(localStorage.getItem('lang'))
        } else {
            localStorage.setItem('lang', lang);
        }
    }, [])

    return (
        <div className={styles.header}>
            <div style={{width: '1640px', background: 'rgba(17, 10, 29, 1)'}}>
                <div className={styles.content}>
                    <div className={styles.logo}>
                    </div>
                    <ul className={styles.navigation_menu}>
                        {links.map((link, key) => {
                            return (
                                <li key={key}>
                                    <a className={styles.menu_link} href={'#'}>
                                        {link}
                                    </a>
                                </li>
                            );
                        })}
                    </ul>
                    <div className={styles.navigation_buttons}>
                        <ButtonNavigation onClick={() => {
                            if(lang === languages.en) {
                                setLang(languages.ru);
                                localStorage.setItem('lang', languages.ru);
                            }
                            else{
                                setLang(languages.en);
                                localStorage.setItem('lang', languages.en);
                            }
                        }} >
                            <img src={lang_ic} width={15} height={15} alt={'lang'}/> {lang}
                        </ButtonNavigation>
                        <ButtonNavigation href={'#'}>
                            Log In
                        </ButtonNavigation>
                        <ButtonNavigation fill={true} href={'#'}>
                            Sign up
                        </ButtonNavigation>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Header;
