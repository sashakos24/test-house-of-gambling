import React, {useState} from 'react';
import styles from './HeaderMobile.module.scss';
import ButtonNavigation from "../../ButtonNavigation";
import {lang_ic} from "../../../assets/images/images";
import {links} from "../../../assets/utils/constants";

const HeaderMobile = () => {

    const [menuOpen, setMenuOpen] = useState(false);

    return (
        <div className={styles.mobile_header}>
            <div className={styles.header_panel}>
                <div className={styles.header}>
                    <div className={styles.panel_logo}>
                    </div>
                    <div className={styles.panel_buttons}>
                        <ButtonNavigation style={{borderRadius: '100%', padding: '6px'}}>
                            <img src={lang_ic} alt={'lang'} width={20}/>
                        </ButtonNavigation>
                        <section className={styles.section}>
                            <input onClick={() => {
                                setMenuOpen(!menuOpen)
                            }
                            } className={styles.displayNone} id="menu-toggle" type="checkbox"/>
                            <label className={styles.menu_button_container} for="menu-toggle">
                                <div
                                    className={styles.menu_button + ' ' + (menuOpen ? styles.menu_button_on : '')}></div>
                            </label>
                        </section>
                    </div>
                </div>
                <ul className={menuOpen ? styles.menu : styles.menu_hide}>
                    {links.map((link, key) =>
                        <li key={key}>{link}</li>
                    )}

                    <ButtonNavigation style={{margin: '30px 0 0 0 ', background: 'rgba(30, 18, 49, 1)', justifyContent: 'center'}} href={'#'}>
                        log in
                    </ButtonNavigation>
                    <ButtonNavigation style={{margin: '0', justifyContent: 'center'}} fill={true} href={'#'}>
                        Sign up
                    </ButtonNavigation>
                </ul>
            </div>
        </div>
    );
};

export default HeaderMobile;
