import React from 'react';
import styles from './Footer.module.scss';
import SocialItem from "./SocialItem";
import {
    tiktok_ic,
    twitter_ic,
    youtube_ic,
    telegram_ic,
    vk_ic,
    linked_ic,
    facebook_ic,
    instagram_ic
} from '../../assets/images/images';

const Footer = () => {
    const pageLinks = ['Terms & Conditions', 'Cookies', 'Contacts', 'Careers', 'Brand Guide'];
    const socials = [twitter_ic, facebook_ic, instagram_ic, linked_ic, youtube_ic, tiktok_ic, telegram_ic, vk_ic,];

    return (
        <div className={styles.footer}>
            <div className={styles.footer_content}>
                <div className={styles.pages}>
                    {pageLinks.map((link, key) => {
                        return (
                            <div key={key} className={styles.page_link}>
                                {link}
                            </div>
                        )
                    })}
                </div>
                <div className={styles.social_media}>
                    <h3 className={styles.media}>
                        Our social media:
                    </h3>
                    <div className={styles.socials}>
                        {socials.map((social, key) => {
                            return (
                                <SocialItem key={key} icon={social}/>
                            )
                        })}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Footer;
