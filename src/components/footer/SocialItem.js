import React from 'react';
import styles from './Footer.module.scss';
import {social_active, social_circle} from "../../assets/images/images";

const SocialItem = ({icon, key}) => {
    return (
        <a onMouseEnter={(e) => {
            e.target.style.backgroundImage = `url(${social_active})`;
            e.target.style.transition = `0.2s linear`;
        }}
           onMouseLeave={(e) => {
               e.target.style.backgroundImage = `url(${social_circle})`;
               e.target.style.transition = `0.2s linear`;
           }}
           style={{backgroundImage: `url(${social_circle})`}}
           className={styles.social_item}
           key={key}
           href={'#'}
        >
            <img src={icon} className={styles.icon_image}/>
        </a>
    );
};

export default SocialItem;
